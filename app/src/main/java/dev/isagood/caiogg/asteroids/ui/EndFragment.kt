package dev.isagood.caiogg.asteroids.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import dev.isagood.caiogg.asteroids.R
import dev.isagood.caiogg.asteroids.databinding.FragmentEndBinding
import dev.isagood.caiogg.asteroids.databinding.FragmentStartBinding
import dev.isagood.caiogg.asteroids.viewmodel.GameViewModel


class EndFragment : Fragment() {

    private var _binding: FragmentEndBinding? = null
    private val binding get() = _binding!!

    private val viewModel: GameViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding = FragmentEndBinding.inflate(inflater, container, false)
        _binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        val startButton = binding.button
        //Set Start Button on click Listener Navigate
        //findNavController().navigate(R.id.action_startFragment_to_gameFragment)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}