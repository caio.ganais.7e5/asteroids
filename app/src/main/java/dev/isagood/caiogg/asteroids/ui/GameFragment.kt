package dev.isagood.caiogg.asteroids.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import dev.isagood.caiogg.asteroids.R
import dev.isagood.caiogg.asteroids.databinding.FragmentStartBinding
import dev.isagood.caiogg.asteroids.game.GameView
import dev.isagood.caiogg.asteroids.viewmodel.GameViewModel


class GameFragment : Fragment() {

    private val viewModel: GameViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val gameView = context?.let { GameView(it, viewModel) }

        return gameView
    }


}
