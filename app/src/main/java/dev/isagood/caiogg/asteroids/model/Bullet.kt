package dev.isagood.caiogg.asteroids.model

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import dev.isagood.caiogg.asteroids.R

data class Bullet(
    val drawable: Drawable,
    var x_position: Float,
    var y_position:Float,
    val speed: Float = 10f
) {
    val bitmap: Bitmap = drawable.toBitmap()
    val rect get() = RectF(x_position, y_position, x_position+bitmap.width, y_position+bitmap.height)

}
