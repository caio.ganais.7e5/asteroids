package dev.isagood.caiogg.asteroids.viewmodel

import android.annotation.SuppressLint
import android.app.Application
import android.graphics.Bitmap
import android.graphics.Point
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dev.isagood.caiogg.asteroids.R
import dev.isagood.caiogg.asteroids.model.Button
import dev.isagood.caiogg.asteroids.model.Enemy
import dev.isagood.caiogg.asteroids.model.Joystick
import dev.isagood.caiogg.asteroids.model.SpaceShip
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlin.random.Random
import kotlin.random.nextInt

class GameViewModel(private val application: Application) : AndroidViewModel(application) {

    val width = application.applicationContext.resources.displayMetrics.widthPixels
    val height = application.applicationContext.resources.displayMetrics.heightPixels

    val button: Button = Button(
        width * 0.8f + 50f,
        height * 0.9f - 50f,
        100f
        )


    val joystick: Joystick = Joystick(
        width * 0.1f + 100f,
        height * 0.9f - 100f,
        200f
    )

    data class GameState(
        val isRunning: Boolean
    )
    val gameLoop = flow<GameState> {
        while (spaceShip.isAlive){
            emit(GameState(isRunning = true))
            delay(10)
        }
        emit(GameState(isRunning = false))
    }

    val spaceShip: SpaceShip = SpaceShip(application.applicationContext.resources.getDrawable(R.drawable.spaceship_icon), x_position = width/4f, y_position = height/2f)

    var enemies: MutableList<Enemy> = mutableListOf()
    fun createEnemy(width: Int, height: Int){
        enemies = enemies.filter { enemy ->
            enemy.x_position >= 0f && enemy.x_position <= width && enemy.isAlive
        }.toMutableList()
        enemies.add(Enemy(application.applicationContext.resources.getDrawable(R.drawable.enemy), x_position = Random.nextFloat() * (width - width/4) + width/4, y_position = Random.nextFloat() * (height- height / 4) + height / 4))
    }
}