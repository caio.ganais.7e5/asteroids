package dev.isagood.caiogg.asteroids.model

import android.view.MotionEvent

class Button(val x: Float, val y: Float, val radius: Float) {

    var isTouched = false

    fun checkIfTouched(x: Float, y: Float) {
        val dx = x - this.x
        val dy = y - this.y
        val distance = Math.sqrt((dx * dx + dy * dy).toDouble())
        isTouched = distance <= radius
    }

    fun resetIfTouched(x: Float, y: Float) {
        val dx = x - this.x
        val dy = y - this.y
        val distance = Math.sqrt((dx * dx + dy * dy).toDouble())
        if (distance <= radius) {
            isTouched = false
        }
    }
}
