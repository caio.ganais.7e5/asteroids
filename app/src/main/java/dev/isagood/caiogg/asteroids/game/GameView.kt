package dev.isagood.caiogg.asteroids.game

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.view.MotionEvent
import android.view.SurfaceView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import dev.isagood.caiogg.asteroids.R
import dev.isagood.caiogg.asteroids.viewmodel.GameViewModel
import kotlinx.coroutines.launch
import kotlin.random.Random


@SuppressLint("ResourceAsColor")
class GameView(context: Context): SurfaceView(context){

    private lateinit var viewModel: GameViewModel

    constructor(context: Context, gameViewModel: GameViewModel) : this(context){
        viewModel = gameViewModel
        (context as LifecycleOwner)
            .lifecycleScope
            .launch {
                viewModel.gameLoop.collect {
                    when (it.isRunning){
                        true -> {
                            invalidate()
                        }
                        false -> {
                            findNavController().navigate(R.id.action_gameFragment_to_endFragment)
                        }
                    }
                }
            }
    }




    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        drawSpaceship(canvas)
        drawEnemies(canvas)
        drawJoystick(canvas)
        drawButton(canvas)
        moveSpaceship()

    }

    private fun moveSpaceship() {
        if (viewModel.joystick.joystickXPosition != viewModel.joystick.baseX || viewModel.joystick.joystickYPosition != viewModel.joystick.baseY) {
            // calculate the angle between joystick and base
            val angle = Math.atan2(
                viewModel.joystick.joystickYPosition - viewModel.joystick.baseY.toDouble(),
                viewModel.joystick.joystickXPosition - viewModel.joystick.baseX.toDouble()
            )

            // move spaceship towards joystick direction
            val speed = 5f
            val newX = Math.cos(angle) * speed
            val newY = Math.sin(angle) * speed
            viewModel.spaceShip.x_position = (newX + viewModel.spaceShip.x_position).toFloat().coerceIn(0f, width.toFloat())
            viewModel.spaceShip.y_position = (newY + viewModel.spaceShip.y_position).toFloat().coerceIn(0f, height.toFloat())


        }
    }

    private fun drawButton(canvas: Canvas) {
        val buttonPaint = Paint().apply {
            color = if (viewModel.button.isTouched) Color.RED else Color.GREEN
            style = Paint.Style.FILL
        }
        canvas.drawCircle(viewModel.button.x, viewModel.button.y, viewModel.button.radius, buttonPaint)
    }




    private fun drawJoystick(canvas: Canvas) {
        // Dibujar la base del joystick
        val joystickCirclePaint = Paint().apply {
            color = Color.LTGRAY
            style = Paint.Style.FILL
        }
        canvas.drawCircle(viewModel.joystick.baseX, viewModel.joystick.baseY, viewModel.joystick.radius, joystickCirclePaint)

        // Dibujar la bola del joystick en la posición adecuada
        val joystickBallPaint = Paint().apply {
            color = Color.WHITE
            style = Paint.Style.FILL
        }
        canvas.drawCircle(
            viewModel.joystick.joystickXPosition,
            viewModel.joystick.joystickYPosition,
            viewModel.joystick.radius * 0.5f,
            joystickBallPaint
        )
    }

    private fun drawSpaceship(canvas: Canvas) {
        canvas.drawBitmap(viewModel.spaceShip.bitmap, viewModel.spaceShip.x_position, viewModel.spaceShip.y_position, null)
        viewModel.spaceShip.bullets.forEach{    bullet ->
            bullet.x_position += bullet.speed
            viewModel.enemies.forEach{enemy ->
            enemy.hitEnemy(bullet)
            }
            canvas.drawBitmap(bullet.bitmap, bullet.x_position, bullet.y_position, null )

        }

    }

    private fun drawEnemies(canvas: Canvas){
        if (viewModel.enemies.size !in 1..10 && Random.nextInt(100) < 5){
            viewModel.createEnemy(width, height)
        }
        viewModel.enemies.forEach { enemy ->

            canvas.drawBitmap(enemy.bitmap, enemy.x_position, enemy.y_position, null)
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event?.actionMasked) {
            MotionEvent.ACTION_DOWN, MotionEvent.ACTION_POINTER_DOWN -> {
                for (i in 0 until event.pointerCount) {
                    val x = event.getX(i)
                    val y = event.getY(i)
                    viewModel.button.checkIfTouched(x, y)
                    if (viewModel.button.isTouched){
                        viewModel.spaceShip.addBullet(width, height, resources)
                    }
                }
            }
            MotionEvent.ACTION_MOVE -> {
                for (i in 0 until event.pointerCount) {
                    val x = event.getX(i)
                    val y = event.getY(i)
                    if (x < width / 2) {
                        viewModel.joystick.joystickXPosition = x
                        viewModel.joystick.joystickYPosition = y
                        viewModel.joystick.limitJoystickPosition(event)
                    }
                }
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_POINTER_UP -> {
                for (i in 0 until event.pointerCount) {
                    val x = event.getX(i)
                    val y = event.getY(i)
                    viewModel.button.resetIfTouched(x, y)
                }
                viewModel.joystick.joystickXPosition = viewModel.joystick.baseX
                viewModel.joystick.joystickYPosition = viewModel.joystick.baseY
            }
        }
        return true
    }


    init {
        setBackground(resources.getDrawable(R.drawable.background, null))
    }
}