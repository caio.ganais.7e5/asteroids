package dev.isagood.caiogg.asteroids.model

import android.view.MotionEvent

class Joystick(val baseX: Float, val baseY: Float, val radius: Float) {

    var joystickXPosition = baseX
    var joystickYPosition = baseY

    fun limitJoystickPosition(event: MotionEvent?) {
        event?.let {
            val dx = it.x - baseX
            val dy = it.y - baseY
            val distance = Math.sqrt((dx * dx + dy * dy).toDouble())
            val maxDistance = radius - 50 // 50 is the radius of the joystick ball

            if (distance > maxDistance) {
                val angle = Math.atan2(dy.toDouble(), dx.toDouble())
                val limitedX = (baseX + maxDistance * Math.cos(angle)).toFloat()
                val limitedY = (baseY + maxDistance * Math.sin(angle)).toFloat()
                joystickXPosition = limitedX
                joystickYPosition = limitedY
            } else {
                joystickXPosition = it.x
                joystickYPosition = it.y
            }
        }
    }
}

