package dev.isagood.caiogg.asteroids.model

import android.graphics.Bitmap
import android.graphics.Rect
import android.graphics.RectF
import android.graphics.drawable.Drawable
import androidx.core.graphics.drawable.toBitmap

data class Enemy(
    val drawable: Drawable,
    var x_position: Float,
    var y_position:Float,
    var isAlive:Boolean = true
){
    val bitmap: Bitmap = drawable.toBitmap()
    val rect get() = RectF(x_position, y_position, x_position+bitmap.width, y_position+bitmap.height)
    fun hitEnemy(bullet: Bullet){
        if (RectF.intersects(rect, bullet.rect)){
            isAlive = false
        }
    }
}