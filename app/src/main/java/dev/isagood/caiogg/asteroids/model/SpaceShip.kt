package dev.isagood.caiogg.asteroids.model

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import androidx.core.graphics.drawable.toBitmap
import dev.isagood.caiogg.asteroids.R

data class SpaceShip(
    val drawable: Drawable,
    var x_position: Float ,
    var y_position:Float ,
    val isAlive:Boolean = true
){
    fun addBullet(width: Int, height: Int, resources: Resources) {
        bullets = bullets.filter { bullet ->
            bullet.x_position >= 0f && bullet.x_position <= width
        }.toMutableList()

        bullets.add(Bullet(drawable = resources.getDrawable(R.drawable.bullet, null), x_position+bitmap.width, y_position+bitmap.height/4))
    }

    var bullets = mutableListOf<Bullet>()
    val bitmap: Bitmap = drawable.toBitmap()
}
